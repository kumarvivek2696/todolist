<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<html>
<head>
<style>

		.message {
	background: #f3f3ff;
	border: 1px solid #b2d1ff;
	color: #006dba;
	   -moz-box-shadow: 0 0 0.25em #b2d1ff;
	-webkit-box-shadow: 0 0 0.25em #b2d1ff;
	        box-shadow: 0 0 0.25em #b2d1ff;
}

h1{
color : #b20000;
}

input[type=submit] {
		    padding:5px 15px; 
    background:#b20000;
    color : #ffffff; 
    border:0 none;
    cursor:pointer;
    -webkit-border-radius: 5px;
    border-radius: 5px; 
		    
		}
		
			ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
a.login{
color : #b20000;
}
li a:hover {
    background-color: #111;
}

label{
color : #b20000;
}
		
</style><title><g:message code="Login Page" args="[entityName]"/></title>
</head>
<body>

<div>
	 <ul>	
  
  <li><a href="register">Sign Up</a></li>
  <li><a href="about">About</a></li>
</ul> 
	
	
	<h1>Log Yourself In !</h1>
	
    </div>  
	<g:if test="${flash.message }">
		<div class="message">
		${flash.message }
		</div>
	</g:if>
	<g:form controller="user" action="login" style="padding-left:100px">
	<div style="width:220px">
		<label>UserName:</label><input type="text" name="userName"/>
		<label>Password:</label><input type="password" name="password"/>
		<br>
		<br>
		<input type="submit" value="Login" name="Login"/>
		<br>
		<br>
		<br>
		<a href="register" class="login">Register yourself with us</a>
		<br/>
		<a href="fb_login" class="login">Login With Facebook</a>
	</div>
	</g:form>
	
	    <fb:login-button autologoutlink="true"></fb:login-button>
  <br/>
<fb:name uid="loggedinuser" useyou="false"></fb:name>
<fb:profile-pic uid="loggedinuser" size="normal" />

<g:facebookConnectJavascript  />
</body>
</html>