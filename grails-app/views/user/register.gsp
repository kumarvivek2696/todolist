<html>
<head>
<style>

		.message {
	background: #f3f3ff;
	border: 1px solid #b2d1ff;
	color: #006dba;
	   -moz-box-shadow: 0 0 0.25em #b2d1ff;
	-webkit-box-shadow: 0 0 0.25em #b2d1ff;
	        box-shadow: 0 0 0.25em #b2d1ff;
}

h1{
color : #b20000;
}

input[type=submit] {
		    padding:5px 15px; 
    background:#b20000;
    color : #ffffff; 
    border:0 none;
    cursor:pointer;
    -webkit-border-radius: 5px;
    border-radius: 5px; 
		    
		}
		
			ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
a.login{
color : #b20000;
}
li a:hover {
    background-color: #111;
}

label{
color : #b20000;
}
		
</style>
<title><g:message code="Register" args="[entityName]"/></title>
</head>
<body>

<div>
	 <ul>	
  
  <li><a href="login">Log In</a></li>
  <li><a href="about">About</a></li>
</ul> 
	
	
	<h1>Register with us !</h1>
	
    </div>  
<g:if test="${flash.message }">
		<div class="message">
		${flash.message }
		</div>
	</g:if>
	<g:form controller="user" action="register" style="padding-left:100px">
	<div style="width:220px">
		<label>Full Name:</label><input type="text" name="fullName"/>
		<label>UserName:</label><input type="text" name="userName"/>
		<label>Password:</label><input type="password" name="password"/>
		<br>
		<br><input type="submit" value="Register" name="Register" />
		
		<br>
		<br><br>
		<a href="login" class="login">Login</a>
	</div>
	</g:form>
</body>
</html>