<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1425007134288562', // App ID
      //channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
    // Additional initialization code here
  };
  // Load the SDK Asynchronously
  (function (data) {
          var jse, id = 'facebook-jssdk', ref = data.getElementsByTagName('script')[0];
          if (data.getElementById(id)) {
              return;
          }
          jse = data.createElement('script');
          jse.id = id;
          jse.async = true;
          jse.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1425007134288562";
          ref.parentNode.insertBefore(jse, ref);
      }(document));
</script>
<script type="text/javascript">
    <!--
        function delayer() {
            window.location = "${createLink(controller:'user', action: 'signin_fb')}"
        }
    //-->
</script>
</head>
<body>


<div id="login_section">
    <h4>Login using Facebook Connect</h4> 
    <div class="login_form">
        <fb:login-button autologoutlink="false" onlogin="setTimeout('delayer()',100)">
        </fb:login-button>
        <br/>
    <fb:name id="userid" uid="loggedinuser" useyou="false"></fb:name>
    <fb:profile-pic uid="loggedinuser" size="normal" />           
    <g:facebookConnectJavascript  />
    </div>
</div> 
</body>
</html>