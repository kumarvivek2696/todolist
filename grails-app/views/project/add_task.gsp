<html>
<head>
<style>
		
		.message {
	background: #f3f3ff;
	border: 1px solid #b2d1ff;
	color: #006dba;
	   -moz-box-shadow: 0 0 0.25em #b2d1ff;
	-webkit-box-shadow: 0 0 0.25em #b2d1ff;
	        box-shadow: 0 0 0.25em #b2d1ff;
}
		
			ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
h1{
color : #b20000;
}

th{
color : #b20000;
}

tr{
color : #b20000;
}

td{
color : #b20000;
}
h3{
color : #b20000;
}

a.login{
color : #b20000;
}
li a:hover {
    background-color: #111;
}
		input[type=submit] {
		    background: url(ic_home_black_24dp_1x.png);
		    /*border: 0;
		    display: block;
		    height: _the_image_height;
		    width: _the_image_width;*/
		}
		</style><title><g:message code="Create Project" args="[entityName]"/></title>
</head>
<body>

<div>
	 <ul>	
  <li><a href="home_page"><asset:image src="ic_add_circle_white_24dp_1x.png" alt="Home"/></a></li>
  <li><a href="project_monitor">Project Monitor</a></li>
  <li><a href="user_profile" class="login"><asset:image src="ic_account_circle_white_24dp_1x.png" alt="Account"/></a></li> 
  <li><a href="about" >About</a></li>
</ul> 
	
	
	<h1>Add Task</h1>
	
    </div>    
	<g:form controller="project" action="add_task" style="padding-left:200px">
	<div style="width:220px">
	<g:if test="${flash.message}">
			<div class="message">
			${flash.message }
			</div>
		</g:if>
		<label>Task Name:</label><input type='text' name='taskName'>
  		<br>
  		<label>Priority:</label><input type="number" name="taskPriority" />
  		<br>
  		<label>Start Date:</label><input type="text" name="taskStartDate" />
  		<br>
  		<label>Due Date:</label><input type="text" name="taskDueDate" />
  		<br>
  		<br>
		<br>
		<input type="submit" value="Add Task" name="addTask"/>
		<br>
		<!-- <g:form controller="project" action="add_task" style="padding-left:200px">
		<input type="submit" value="Add Another Task" name="addAnotherTask"/>
		</g:form> -->
		<br>
		<g:form controller="project" action="project_monitor" style="padding-left:200px">
		<input type="submit" value="Project Monitor" name="addAnotherTask"/>
		</g:form>
		
	</div>
	</g:form>
</body>
</html>