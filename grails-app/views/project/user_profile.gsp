<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

		
		.message {
	background: #f3f3ff;
	border: 1px solid #b2d1ff;
	color: #006dba;
	   -moz-box-shadow: 0 0 0.25em #b2d1ff;
	-webkit-box-shadow: 0 0 0.25em #b2d1ff;
	        box-shadow: 0 0 0.25em #b2d1ff;
}
		
			ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

li a:hover {
    background-color: #111;
}
		input[type=submit] {
		    background: url(ic_home_black_24dp_1x.png);
		    /*border: 0;
		    display: block;
		    height: _the_image_height;
		    width: _the_image_width;*/
		}
		
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  margin: auto;
  text-align: center;
  font-family: arial;
  background-color : #c20000;
color:white;
  
}

.title {
  color: grey;
  font-size: 18px;
}
h2{
color:#c20000;
}
button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}
a.logout
{
border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 94.8%;
  font-size: 18px;
}
a {
  text-decoration: none;
  //font-size: 22px;
  color: black;
}

button:hover, a:hover {
  opacity: 0.7;
}
</style>
</head>
<body>
	 <ul>	
  <li><a href="home_page"><asset:image src="ic_add_circle_white_24dp_1x.png" alt="Home"/></a></li>
  <li><a href="project_monitor">Project Monitor</a></li>
  <li><a href="user_profile"><asset:image src="ic_account_circle_white_24dp_1x.png" alt="Account"/></a></li> 
  <li><a href="about">About</a></li>
</ul> 
	
	
<h2 style="text-align:center">Here is your User Profile Card</h2>

<g:each in="${y}">
<div class="card">

  <br>
  <asset:image src="ic_face_white_24dp_2x.png" alt="Name"/>
  <h1>${it.full_name} </h1>
  <h3>@${it.user_name} </h3>
  <p class="title"></p>
  <div style="margin: 24px 0;">
    <a href="#"><i class="fa fa-dribbble"></i></a> 
    <a href="#"><i class="fa fa-twitter"></i></a>  
    <a href="#"><i class="fa fa-linkedin"></i></a>  
    <a href="#"><i class="fa fa-facebook"></i></a> 
 </div>
 
<button>Contact</button>
<br>
<a href="logout" class = "logout">Logout</a>
</div>

 </g:each>
        

</body>
</html>
