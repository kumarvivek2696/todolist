<!DOCTYPE html>
<html>
	<head>

		<style>
		
		.message {
	background: #f3f3ff;
	border: 1px solid #b2d1ff;
	color: #006dba;
	   -moz-box-shadow: 0 0 0.25em #b2d1ff;
	-webkit-box-shadow: 0 0 0.25em #b2d1ff;
	        box-shadow: 0 0 0.25em #b2d1ff;
}
		
			ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
h1{
color : #b20000;
}

th{
color : #b20000;
}

tr{
color : #b20000;
}

td{
color : #b20000;
}
h3{
color : #b20000;
}

a.login{
color : #b20000;
}
li a:hover {
    background-color: #111;
}
		input[type=submit] {
		    background: url(ic_home_black_24dp_1x.png);
		    /*border: 0;
		    display: block;
		    height: _the_image_height;
		    width: _the_image_width;*/
		}
		</style>
		<title>Project Monitor</title>
	</head>
	<body>
	
	<div>
	 <ul>	
  <li><a href="home_page"><asset:image src="ic_add_circle_white_24dp_1x.png" alt="Home"/></a></li>
  <li><a href="project_monitor">Project Monitor</a></li>
  <li><a href="user_profile" class="login"><asset:image src="ic_account_circle_white_24dp_1x.png" alt="Account"/></a></li> 
  <li><a href="about" >About</a></li>
</ul> 
	
	
	<h1>Project Monitor</h1>
	
    </div>    
		
	<br>
	<table cellspacing=4>
		<tr>
    		<th align = "center">Project Name</th>
    		<th align = "center">Project Start Date</th>
    		<th align = "center">Project End Date</th>
    		<th align = "center">Number of Tasks</th>
    		<th align = "center">Project Status</th>
    		<th align = "center">Delete</th>  
    		<th align = "center">View</th>
    		<th align = "center">Done</th>
		</tr>
        <g:each in="${y}">
        	<tr>
        	    <td align = "center">${it.project_name}</td>
        	    <td align = "center">${it.project_start_date}</td>
        	    <td align = "center">${it.project_deadline}</td>
        	    <td align = "center">${it.project_task_count}</td>
        	    <td align = "center">${it.project_status}</td>
        	    <td align = "center"><a class="login" href='delete_project?id=${it.id}' >delete</a></td>
        	    <td align = "center"><a class="login" href='task_monitor?id=${it.id}'>view</a></td> 
       		    <td align = "center"><a class="login" href='done_project?id=${it.id}'>done</a></td> 
       		
       		</tr> 
       		
       		
            <br/>
        </g:each>
        </table>
        
        <br>
        <br>
      	<h3>OverDue</h3>
      	<table border=1>
		<tr>
    		<th align = "center">Project Name</th>
    		<th align = "center">Project Start Date</th>
    		<th align = "center">Project End Date</th>
    		<th align = "center">Number of Tasks</th> 
    		<th align = "center">Delete</th>  
    		<th align = "center">View</th>
		</tr>
        <g:each in="${x}">
        	<tr>
        	    <td align = "center">${it.project_name}</td>
        	    <td align = "center">${it.project_start_date}</td>
        	    <td align = "center">${it.project_deadline}</td>
        	    <td align = "center">${it.project_task_count}</td>
        	    <td align = "center"><a href='delete_project?id=${it.id}' class="login">delete</a></td>
        	    <td align = "center"><a href='task_monitor?id=${it.id}' class="login">view</a></td> 
       		</tr> 
            <br/>
        </g:each>
        </table>
    
      	
	</body>
</html>