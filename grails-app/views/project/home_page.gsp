<html>
<head>
<style>
		
		.message {
	background: #f3f3ff;
	border: 1px solid #b2d1ff;
	color: #006dba;
	   -moz-box-shadow: 0 0 0.25em #b2d1ff;
	-webkit-box-shadow: 0 0 0.25em #b2d1ff;
	        box-shadow: 0 0 0.25em #b2d1ff;
}
		
			ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
h1{
color : #b20000;
}

th{
color : #b20000;
}

tr{
color : #b20000;
}

td{
color : #b20000;
}
h3{
color : #b20000;
}

a.login{
color : #b20000;
}

label{
color : #b20000;

}
li a:hover {
    background-color: #111;
}
		
		</style>
<title><g:message code="Home" args="[entityName]"/></title>
</head>
<body>
	 <ul>	
  <li><a href="home_page"><asset:image src="ic_add_circle_white_24dp_1x.png" alt="Home"/></a></li>
  <li><a href="project_monitor">Project Monitor</a></li>
  <li><a href="logout">Logout</a></li>
  <li><a href="user_profile"><asset:image src="ic_account_circle_white_24dp_1x.png" alt="Account"/></a></li> 
  <li><a href="about">About</a></li>
</ul> 
	
	<h1>Add Project</h1>
	
	<g:form controller="project" action="home_page" style="padding-left:200px">
	
	<div style="width:220px">
		<label>Project Name:</label><input type="text" name="projectName"/>
		<br>
		<label>Project Type:</label><g:countrySelect name="projectType"
    		value="${projectType}"
    		from="['personal', 'shopping', 'work','Movies']"
    		noSelection="['':'-Choose project type-']"
    		valueMessagePrefix="projectType" />
		<br>
		<label>Project Start Date:</label><input type="text" name="projectStartDate" placeHolder="yyyy/MM/dd"/>
		<br>
		<label>Project End Date:</label><input type="text" name="projectEndDate" placeHolder="yyyy/MM/dd"/>
		
		<br>
		<label>Project Priority:</label><input type="number" name="projectPriority" max="5"/>
		<br>
		<br>
		<g:if test="${flash.message }">
			<div class="message">
			${flash.message }
			</div>
		</g:if>
		<br>
		<input type="submit" value="Create Project" name="Create"/>
	</div>
	</g:form>
	<br>
	
	
</body>
</html>