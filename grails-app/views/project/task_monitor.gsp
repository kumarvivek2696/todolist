<!DOCTYPE html>
<html>
	<head>
	
	<style>
		
		.message {
	background: #f3f3ff;
	border: 1px solid #b2d1ff;
	color: #006dba;
	   -moz-box-shadow: 0 0 0.25em #b2d1ff;
	-webkit-box-shadow: 0 0 0.25em #b2d1ff;
	        box-shadow: 0 0 0.25em #b2d1ff;
}
		
			ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
h1{
color : #b20000;
}

th{
color : #b20000;
}

tr{
color : #b20000;
}

td{
color : #b20000;
}
h3{
color : #b20000;
}

a.login{
color : #b20000;
}
li a:hover {
    background-color: #111;
}
		
		</style>
		<title>Task Monitor</title>
	</head>
	<body>
	
 <ul>	
  <li><a href="home_page"><asset:image src="ic_add_circle_white_24dp_1x.png" alt="Home"/></a></li>
  <li><a href="project_monitor">Project Monitor</a></li>
  <li><a href="user_profile" class="login"><asset:image src="ic_account_circle_white_24dp_1x.png" alt="Account"/></a></li> 
  <li><a href="about" >About</a></li>
</ul> 
	
	
	
	<h1>Task Monitor</h1>
	
	<table>
		<tr>
    		<th>Task Name</th>
    		<th>Task Start Date</th>
    		<th>Task End Date</th>
    		<th>Status</th> 
    		<th>Delete</th>  
    		<th>View</th>
		</tr>
        <g:each in="${task_info}">
        	<tr>
        	    <td>${it.task_name}</td>
        	    <td>${it.start_date}</td>
        	    <td>${it.due_date}</td>
        	    <td>${it.task_status}</td>
        	    <td><a href='delete_task?id=${it.id}' class="login">delete</a></td>
        	    <td><a href='done_task?id=${it.id}' class="login">done</a></td> 
       		</tr> 
        </g:each>
        </table>
        
        <g:form controller="project" action="project_monitor" style="padding-left:200px">
		<input type="submit" value="Project Monitor" name="home"/>
		</g:form>
		<g:form controller="project" action="add_task" style="padding-left:200px">
		<input type="submit" value="Add Task" name="addAnotherTask"/>
		</g:form>
		
		
	</body>
</html>