package todolist

class Task {

	static mapping={
		table 'todolist_task'
	}
	String  task_name, priority, assigned_to,start_date, due_date, send_push_notifications, task_status, user_id, project_id
	
    static constraints = {
    }
}
