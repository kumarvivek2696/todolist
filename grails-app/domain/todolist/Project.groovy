package todolist

class Project {

	static mapping = {
		table 'project'
	}
	
	String toString()
	{
		return id
	}
	String project_name
	String project_type,project_deadline
	String project_start_date
	Integer project_priority,project_task_count
	Integer user_id
	Integer project_status
	static constraints = {
	
	}
}
