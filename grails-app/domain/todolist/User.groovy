package todolist

class User {
	static mapping = {
        table 'todolist_user'
       // id generator: 'assigned'
//		id generator: 'hilo',
//		params: [table: 'todolist_user', column: 'user_id', max_lo: 100]
//        firstName column: 'First_Name'
//        addresses lazy: false
    }
	String user_name,full_name,password,login_medium
	String last_login_date
	String user_creation_date,record_date
	String last_modified_date
	Integer current_status
	
    static constraints = {
		user_name(unique:true)
		password(password:true)
		    }
}
