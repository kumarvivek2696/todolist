package todolist


class ProjectController {

	def static current_project_id, current_project_name
	def static project_info=new ArrayList<String>()
    def index() { redirect(action:"home_page")}
	def home_page={
		
			def  session_user= CUser.findAll()
			if(session_user.size()==0)
			{
				flash.message="Session Expired! Please Login!"
				redirect(controller:"user",action:"login")
			}
			else
			{
			println(params.projectName)
			if(params.projectName==null)
			{
				flash.message="Enter Project Name!"
			}
			else if(params.projectType==null)
			{
				flash.message="Enter Project Type!"
			}
			else if(params.projectStartDate==null)
			{
				flash.message="Enter Project Start Date!"
			}
			else if(params.projectEndDate==null)
			{
				flash.message="Enter Project End Date!"
			}
			else if(params.projectPriority==null)
			{
				flash.message="Enter Project Priority!"
			}
			else{
				
				try {
					
					Date.parse('yyyy/MM/dd', params.projectEndDate)
					println('here')
					Date.parse('yyyy/MM/dd', params.projectStartDate)
							
					Integer user_id
					def z = CUser.findAll()
					for (j in z)
					{
						user_id = j.user_id
					}
			
			
				println(user_id)
				def project_row=new Project(project_name:params.projectName, project_type:params.projectType,project_deadline:params.projectEndDate,project_start_date:params.projectStartDate, project_priority:params.projectPriority,project_task_count:0,
					user_id:user_id, project_status:0)
				def x=project_row.save(failOnError:true)
				println(x)
				
				if(x)
				{
					project_info=new ArrayList<String>()
					project_info.add(params.projectName)
					project_info.add(params.taskNumber)
					def project_id_full=x.toString()
					println(project_id_full.trim())
					project_info.add(project_id_full.trim())
					redirect(action:"add_task")
				}
				else
				{
					
				}
				}
			
				 catch(java.text.ParseException p) {
					flash.message = "Enter valid date"
				}
				}
		
			}
	}
	
	
	
	def add_task={
		
		
		def  session_user= CUser.findAll()
		if(session_user.size()==0)
		{
			flash.message="Session Expired! Please Login!"
			redirect(controller:"user",action:"login")
		}
		else
		{
		String project_name=""
		String project_id=""
		
		if(project_info)
		{
		print(project_info)
		project_name=project_info.get(0)
		project_id=project_info.get(2)
		}
		else if(current_project_id)
		{
			project_name = current_project_name
			project_id = current_project_id
			print(project_name)
			print( project_id)
		}
		println("name:"+params["taskName"])
		if(params["taskName"]==null)
		{	
			
		}
		else
		{
			try {
				
				Date.parse('yyyy/MM/dd', params.taskDueDate)
				println('here')
				Date.parse('yyyy/MM/dd', params.taskStartDate)
			
			Integer user_id
			def z = CUser.findAll()
			for (j in z)
			{
				user_id = j.user_id
			}
			
				//String  task_name, priority, assigned_to, due_date, send_push_notifications, task_status, user_id, project_id
				println("task-"+params["taskName"])
				def task_row=new Task(task_name:params["taskName"],priority:params["taskPriority"]
					,assigned_to:user_id,due_date:params["taskDueDate"],start_date:params["taskStartDate"],
					send_push_notifications: "0",task_status: "0",user_id: user_id,project_id:project_id)
				def x=task_row.save(failOnError:true,flush:true)
				if(x)
				{
					flash.message="Task Added!"
					def project_row=Project.get(project_id)
					println("a"+project_row)
//					for(pro in project_row)
//					{
						println(project_row.project_task_count)
						project_row.project_task_count=project_row.project_task_count+1
						println(project_row.project_task_count)
						project_row.save(failOnError:true,flush:true)
				
			//		}
				//	project_row.save(failOnError:true)
				}
		}
		catch(Exception e)
		{
			flash.message = "Enter valid date"
		}
		
		}
		}
		
	}
	
	def project_monitor={
		
		def  session_user= CUser.findAll()
		if(session_user.size()==0)
		{
			flash.message="Session Expired! Please Login!"
			redirect(controller:"user",action:"login")
		}
		else
		{
		Integer user_id
		println("pro")
//		if(!session.user)
//			{
//				for (def user in session.user) {
//					println(user.full_name)
//					}
//				flash.message="Session Expired! Please Login!"
//				redirect(controller:"user",action:"login")
//			}
//		else
//		{
		
		    def z = CUser.findAll()
			for (j in z)
			{
				user_id = j.user_id
			}
			def y=Project.findAllByUser_id(user_id)
			def today_date = new Date()
			println(y.project_deadline)
			def x = []
			
			
			for (i in y){
				def date = Date.parse("yyyy/MM/dd", i.project_deadline)
				def project_status = i.project_status
				print(date.getTime())
				println(today_date.getTime())
				if(date.getTime() < today_date.getTime() && project_status == 0)
				{
					x.add(i)
				}
			}
			
			
			print(x)
			return [y:y,x:x]
		}
	}
	def logout={
			
		def  session_user= CUser.findAll()
		for(i in session_user)
		{
			CUser.executeUpdate("delete CUser c where c.id = :id", [id:i.id])
		}
		
		//delete
		redirect(action:"login",controller:"user")
	
	}
	
	def task_monitor={
		def  session_user= CUser.findAll()
		if(session_user.size()==0)
		{
			flash.message="Session Expired! Please Login!"
			redirect(controller:"user",action:"login")
		}
		else
		{
		def project_id=params.id
		def project_info = Project.findAllById(project_id)
		print(project_info.project_name)
		current_project_id = project_id
		current_project_name = project_info.project_name
		def task_info=Task.findAllByProject_id(project_id)
		if(task_info)
		{
		println("task_info"+task_info.size())
		return [task_info:task_info]
		}
		}
		
	}
	
	
	def user_profile={
		def  session_user= CUser.findAll()
		if(session_user.size()==0)
		{
			flash.message="Session Expired! Please Login!"
			redirect(controller:"user",action:"login")
		}
		else
		{
		
		Integer user_id
		
		def z = CUser.findAll()
		for (j in z)
		{
			user_id = j.user_id
		}
		
		def y=User.findAllById(user_id)
		print('user'+y)
		
		return [y:y]
		}
			
	}
	def delete_task={
		def  session_user= CUser.findAll()
		if(session_user.size()==0)
		{
			flash.message="Session Expired! Please Login!"
			redirect(controller:"user",action:"login")
		}
		else
		{
		println(params.id)
		Task.executeUpdate("delete Task c where c.id = :id", [id:Long.parseLong(params.id)])
		}
			
	}
	
	def delete_project={
		def  session_user= CUser.findAll()
		if(session_user.size()==0)
		{
			flash.message="Session Expired! Please Login!"
			redirect(controller:"user",action:"login")
		}
		else
		{
		Project.executeUpdate("delete Project c where c.id = :id", [id:Long.parseLong(params.id)])
		}	
		}
	
	def done_task={
		def  session_user= CUser.findAll()
		if(session_user.size()==0)
		{
			flash.message="Session Expired! Please Login!"
			redirect(controller:"user",action:"login")
		}
		else
		{
		
		println(params.id)
		Task.executeUpdate("update Task c set c.task_status = 1 where c.id = :id", [id:Long.parseLong(params.id)])	
		
		def task_info=Task.findAllById(params.id)
		def project_id
		if(task_info)
		{
			project_id = task_info.project_id
			def all_task_info=Task.findAllByProject_id(project_id)
			def flag = 1
			for(i in all_task_info)
			{
				if(i.task_status == 0)
				{
					flag = 0
					break
				}
			}
			
			if(flag == 1)
			{
				def pro_id = Long.parseLong(project_id[0])
				Project.executeUpdate("update Project c set c.project_status = 1 where c.id = :id", [id:pro_id])
			}
		}
		}
			
	}
	def done_project={
		def  session_user= CUser.findAll()
		if(session_user.size()==0)
		{
			flash.message="Session Expired! Please Login!"
			redirect(controller:"user",action:"login")
		}
		else
		{
			def project_id = params.id
		def all_task_info=Task.findAllByProject_id(project_id)
		for(i in all_task_info)
		{
			Task.executeUpdate("update Task c set c.task_status = 1 where c.id = :id", [id:i.id])
		}
		def pro_id = Long.parseLong(project_id)
		Project.executeUpdate("update Project c set c.project_status = 1 where c.id = :id", [id:pro_id])
			}
	
	}
	
}

